;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

[set_color]
;HEX COLORS IN AARRGGBB

black = #FF000000 
white = #dadada
clear = #00fedcba

red = #f50a4d
orange0 = #ffb52a
orange1 = #f5a70a
rose = #ff5555
green = #55aa55

[colors]
foreground = ${set_color.white}
background = ${set_color.black}

bar_default = ${set_color.white}
bar_warm = ${set_color.red}

transparent = ${set_color.clear} 

primary = ${set_color.black}
secondary = ${set_color.black}
alert = ${set_color.red}

[bar/exbar]
;to use polybar-msg
enable-ipc = true

;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 25
;Properties

dpi-x=0
dpi-y=0

;offset-x = 0%
;offset-y = 0%
;Change standar X/Y position

radius = 0.0
;Set the curve of the corners

fixed-center = true 
;Center the modules

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = ${set_color.black}
;See the line below certain modules

border-size = 0 

padding-left = 1
padding-right = 1
;Set maring between
;bar limit and modules

module-margin-left = 1
module-margin-right = 0 
;Set margin between modules

font-0 = Fantasque Sans Mono:style=Bold:size=10;2
font-1 = Material Icons:style=Regular:size=12;3
font-2 = Font Awesome 5 Brands:style=Regular:size=12;2
font-3 = Font Awesome 5 Free:style=Solid:size=12;2
font-4 = Weather Icons:style:Regular:size=12;3
font-5 = Font Awesome 5 Free Solid:style=Solid:size=11;2
font-6 = Material Icons:style=Regular:size=15;4

modules-left = battery cpu temp-cpu1 memory pulseaudio bluetooth 
modules-center = mic date bspwm 
modules-right = temp-gpu wlan backlight   

tray-position = left
tray-padding = 2
tray-background = ${set_color.black}

;wm-restack = bspwm
;wm-restack = i3

;this to tell your WM not to reserve space automaticly
;then u have to set the space to the bar manually
;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

locale = es_PE.UTF-8

;{
;INDICADOR DE BATERIA 
[module/battery]
type = internal/battery
battery = BAT1
adapter = ADP1
full-at = 90 

format-charging = [ %{T4}<animation-charging>%{T-} <label-charging> ]

format-discharging = [ %{T4}<ramp-capacity>%{T-} <label-discharging> ]

label-full = [ %{T5}%{T-} FULL ]
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
;ramp-capacity-foreground = ${self.color1}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-framerate = 1000
;animation-charging-foreground = ${self.color1}
;}

;{
;USO DEL CPU EN PROM 
[module/cpu]
type = internal/cpu
interval = 1.0
label = [ %{T7}%{T-} %percentage%%

;TEMPERATURA DEL CPU 
[module/temp-cpu1]
type = internal/temperature
hwmon-path = /sys/class/hwmon/hwmon5/temp1_input

base-temperature = 35
warn-temperature = 80

format = %{T6}<ramp>%{T-} <label>
format-warn = %{T6}<ramp>%{T-} <label-warn>
format-warn-foreground = ${colors.bar_warm}

label = %temperature-c% ]
label-warn = ${self.label}

ramp-0 = 
ramp-1 = 
ramp-2 =  
ramp-3 =  
ramp-4 = 

;USO DE LA MEMORIA RAM
[module/memory]
type = internal/memory
interval = 1.0
label = [ %{T2}%{T-} %percentage_used%% ]
;}

;{
;WORKSPACES DE BSPWM
	[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.foreground}
label-focused-foreground = ${set_color.black}
label-focused-underline= ${set_color.black}
label-focused-padding = 1

label-occupied = %index%
label-occupied-padding = 1

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

label-empty = %index%
label-empty-foreground = ${colors.foreground}
label-empty-padding = 1
;}


;{
;WIFI INFORMATION
[module/wlan]
type = internal/network
interface = wlp4s0
;	NOTE: u have to configure which interface u r using
interval = 1.5

label-connected = [ U:%upspeed% D:%downspeed%  %{T2}%{T-} %signal% %essid% %{T7}%{T-} %local_ip% ]
label-disconnected = [ %{T2}%{T-} %ifname% disconnected ]
format-disconnected-underline = ${set_color.red}
;}

;{
;FECHA
[module/date]
type = internal/date
interval = 1.0
date = "%A %d %B"
time = %r
label = [ %date% %{T7}%{T-} %time% ]
;}

;{
;AUDIO_SOUND
[module/pulseaudio]
type = internal/pulseaudio

format-volume = [ %{T7}<ramp-volume>%{T-} <bar-volume> <label-volume> ]

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

bar-volume-width = 10
bar-volume-foreground-0 = ${set_color.green} 
bar-volume-foreground-1 = ${set_color.green}
bar-volume-foreground-2 = ${set_color.green}
bar-volume-foreground-3 = ${set_color.green}
bar-volume-foreground-4 = ${set_color.green} 
bar-volume-foreground-5 = ${set_color.green} 
bar-volume-foreground-6 = ${set_color.orange0}
bar-volume-foreground-7 = ${set_color.orange1}
bar-volume-foreground-8 = ${set_color.rose}
bar-volume-foreground-9 = ${set_color.red}
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2

label-volume = %percentage%%
;label-volume-foreground = ${colors.foreground}

format-muted = [ <label-muted> ]

label-muted =  sound muted %percentage%% 
;label-muted-foreground = ${colors.foreground}

;MICROPHONE
[module/mic]
type = custom/script
exec = ~/.custom/scripts/mic_state.sh
interval = 1.0
;}

;{
;BLUETOOTH POWER STATE
[module/bluetooth]
type = custom/script
exec = ~/.custom/scripts/bluetooth.sh
interval = 1.5
;}

;{
;TEMPERATURA DE LA GPU 
[module/label-gpu]
type = custom/text
content = [ AMD: 

[module/temp-gpu]
type = internal/temperature
hwmon-path = /sys/class/hwmon/hwmon3/temp1_input

base-temperature = 20
warn-temperature = 70

format = <ramp>%{T-} <label>
format-warn = <ramp>%{T-} <label-warn>
format-warn-underline = ${self.format-underline}


label = %temperature-c% ]
label-warn = ${self.label}
label-warn-foreground = ${colors.bar_warm}

ramp-0 = [ %{T6}
ramp-1 = [ %{T6}
ramp-2 = [ %{T6}
ramp-3 = [ %{T6}
ramp-4 = [ %{T6}
ramp-foreground = ${colors.foreground}
;}

;{
;BRILLO DE LA PANTALLA
[module/backlight]
type = internal/backlight
card = amdgpu_bl0

format = [ %{T5}<ramp>%{T-} ]

ramp-0 = 
ramp-1 = 
ramp-2 =  
ramp-3 = 
ramp-4 = 
ramp-5 =  
ramp-6 = 
ramp-7 = 
ramp-8 = 
ramp-9 = 
ramp-10 =  
ramp-11 = 
ramp-12 = 
ramp-13 = 
ramp-14 = 

; vim:ft=dosini

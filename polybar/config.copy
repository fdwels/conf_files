;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini

[set_color]
;HEX COLORS IN AARRGGBB

black = #FF000000 
white = #DFffffff
clear = #00fedcba

red = #FFf50a4d
orange0 = #FFffb52a
orange1 = #FFf5a70a
rose = #FFff5555
green = #FF55aa55

yellow = #FFffee00
yellow_tray = #FFfac800

grey = #99000000 
pink = #ffff63ff

[colors]
foreground = ${set_color.white}
background = ${set_color.black}

bar_default = ${set_color.white}
bar_warm = ${set_color.red}

transparent = ${set_color.clear} 
;background-alt = ${set_color.clear}
;foreground-alt = ${set_color.white}


primary = ${set_color.black}
;See the line below workspace selected
secondary = ${set_color.black}
;See the line below certain modules 
alert = ${set_color.red}

[bar/exbar]
;to use polybar-msg
enable-ipc = true

;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 25
;Properties

offset-x = 0%
offset-y = 0%
;Change standar X/Y position

radius = 0.0
;Set the curve of the corners

fixed-center = true 
;Center the modules

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #ff0000000
;See the line below certain modules

border-size = 0 
border-color = #ff000000
;Put a bordar aditional to the bar

padding-left = 1 
padding-right = 1
;Space between bar and the bar

module-margin-left = 1
module-margin-right = 1 
;Set margin between spaces

font-0 = DejaVu Sans:size=10;2
font-1 = Material Icons:style=Regular:size=12;3
font-2 = Font Awesome 5 Brands Regular:style=Regular:size=12;2
font-3 = Font Awesome 5 Free Regular:style=Regular:size=12;2
font-4 = Font Awesome 5 Free Solid:style=Solid:size=12;2
font-5 = Weather Icons:style:Regular:size=12;2
font-6 = siji:pixelsize=10;2
font-7 = Font Awesome 5 Free Solid:style=Solid:size=11;2
font-8 = Material Icons:style=Regular:size=15;4

modules-left = battery cpu temp-cpu1 memory pulseaudio 
modules-center = mic date bspwm
modules-right = label-gpu temp-gpu wlan wlan-info backlight   
;Does not work here i3 filesystem xkeyboard pulseaudio xbacklight eth temperature wlan
;Dont want to use powermenu
;Select the modules u wanna use and set it's position

tray-position = left
tray-padding = 2
tray-background = ${set_color.pink}

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

locale = es_PE.UTF-8

;{
;INDICADOR DE BATERIA 
[module/battery]
type = internal/battery
battery = BAT1
adapter = ADP1
full-at = 90 

;format-charging-foreground = ${set_color.yellow_tray}
format-charging = [ BAT:  %{T5}<animation-charging>%{T-} <label-charging> ]
;format-charging-underline = ${set_color.yellow}

;format-discharging-foreground = ${self.color1}
format-discharging = [ BAT:  %{T5}<ramp-capacity>%{T-} <label-discharging> ]
;format-discharging-underline = ${self.color1}

label-full = [ %{T6}%{T-} FULL ]
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
;ramp-capacity-foreground = ${self.color1}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-framerate = 1000
;animation-charging-foreground = ${self.color1}
;}

;{
;USO DEL CPU EN PROM 
[module/cpu]
type = internal/cpu
interval = 1
label = [ CPU:  %{T9}%{T-} %percentage%%

;TEMPERATURA DEL CPU 
[module/temp-cpu1]
type = internal/temperature
hwmon-path = /sys/class/hwmon/hwmon5/temp1_input

base-temperature = 35
warn-temperature = 80

format = %{T8}<ramp>%{T-} <label>
format-warn = %{T8}<ramp>%{T-} <label-warn>
format-warn-foreground = ${colors.bar_warm}

label = %temperature-c% ]
label-warn = ${self.label}

ramp-0 = 
ramp-1 = 
ramp-2 =  
ramp-3 =  
ramp-4 = 

;USO DE LA MEMORIA RAM
[module/memory]
type = internal/memory
interval = 1
label = [ RAM:  %{T2} %{T-} %percentage_used%% ]
;}

;{
;WORKSPACES DE BSPWM
[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.transparent}
label-focused-foreground = ${set_color.black}
label-focused-underline= ${set_color.black}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground}
label-empty-padding = 2
;}


;{
;WIFI INFORMATION
[module/wlan]
type = internal/network
interface = wlp4s0
;	NOTE: u have to configure which interface u r using
interval = 1.5

;format-connected = <label-connected>
;format-connected-underline = #000000
label-connected = [ %upspeed% %downspeed% %{T2}%{T-} %signal%

label-disconnected = [ %{T2}%{T-} %ifname% disconnected
;format-disconnected = <label-disconnected>
format-disconnected-underline = ${set_color.red}

;WIFI INFORMATION
[module/wlan-info]
type = internal/network
interface = wlp4s0
interval = 3.0

;format-connected = <label-connected>
;format-connected-underline = #000000
label-connected = %essid% %{T2}%{T-} %local_ip% ]

label-disconnected = %{T5}%{T-} ]
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${set_color.red}
;}

;{
;FECHA
[module/date]
type = internal/date
interval = 1.0
date = "%A %d %B"
time = %r
label = [ %date% %{T2}%{T-} %time% ]
;}

;{
;AUDIO_SOUND
[module/pulseaudio]
type = internal/pulseaudio

format-volume = [ <ramp-volume> <bar-volume> <label-volume> ]

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

bar-volume-width = 10
bar-volume-foreground-0 = ${set_color.green} 
bar-volume-foreground-1 = ${set_color.green}
bar-volume-foreground-2 = ${set_color.green}
bar-volume-foreground-3 = ${set_color.green}
bar-volume-foreground-4 = ${set_color.green} 
bar-volume-foreground-5 = ${set_color.green} 
bar-volume-foreground-6 = ${set_color.orange0}
bar-volume-foreground-7 = ${set_color.orange1}
bar-volume-foreground-8 = ${set_color.rose}
bar-volume-foreground-9 = ${set_color.red}
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2

label-volume = %percentage%%
;label-volume-foreground = ${colors.foreground}

format-muted = [ <label-muted> ]

label-muted =  sound muted %percentage%% 
;label-muted-foreground = ${colors.foreground}

;MICROPHONE
[module/mic]
type = custom/script
exec = ~/.custom/scripts/mic_state.sh
interval = 0.5
;}

;DEPRECATED
[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = Volume
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground}

bar-volume-underline = #000000

format-prefix = 
format-prefix-foreground = ${colors.foreground}
format-underline = #000000
;}

;{
;TEMPERATURA DE LA GPU 
[module/label-gpu]
type = custom/text
content = [ GPU: 

[module/temp-gpu]
type = internal/temperature
hwmon-path = /sys/class/hwmon/hwmon3/temp1_input

base-temperature = 20
warn-temperature = 70

format = %{T8}<ramp>%{T-} <label>
format-warn = %{T8}<ramp>%{T-} <label-warn>
format-warn-underline = ${self.format-underline}


label = %temperature-c% ]
label-warn = ${self.label}
label-warn-foreground = ${colors.bar_warm}

ramp-0 = 
ramp-1 = 
ramp-2 =  
ramp-3 =  
ramp-4 = 
ramp-foreground = ${colors.foreground}
;}

;{
;}

;{
;BRILLO DE LA PANTALLA
[module/backlight]
type = internal/backlight
card = amdgpu_bl0

format = [ %{T6}<ramp>%{T-} ]

ramp-0 = 
ramp-1 = 
ramp-2 =  
ramp-3 = 
ramp-4 = 
ramp-5 =  
ramp-6 = 
ramp-7 = 
ramp-8 = 
ramp-9 = 
ramp-10 =  
ramp-11 = 
ramp-12 = 
ramp-13 = 
ramp-14 = 
;ramp-foreground = ${colors.foreground}
;}

;{
;MENU DE APAGADO
;[module/powermenu]
;type = custom/menu

;expand-right = true

;format-spacing = 1

;label-open = 
;label-open-foreground = ${colors.secondary}
;label-close =  cancel
;label-close-foreground = ${colors.secondary}
;label-separator = |
;label-separator-foreground = ${colors.foreground}

;menu-0-0 = reboot
;menu-0-0-exec = menu-open-1
;menu-0-1 = power off
;menu-0-1-exec = menu-open-2

;menu-1-0 = cancel
;menu-1-0-exec = menu-open-0
;menu-1-1 = reboot
;menu-1-1-exec = sudo reboot

;menu-2-0 = power off
;menu-2-0-exec = sudo poweroff
;menu-2-1 = cancel
;menu-2-1-exec = menu-open-0
;}
